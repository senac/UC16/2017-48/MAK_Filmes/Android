# Project Title

Mak Filmes


### Prerequisites

Obter um android acima de 5.1.


## Built With

* [Android Studio](hhttps://developer.android.com/studio/ -)
* [Net Beans](https://maven.apache.org/) - Dependency Management



## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://githttps://gitlab.com/senac/UC16/2017-48/MAK_Filmes/Android

## Authors

Kaique Amorim
Alexandre Souza
Mauyr Monfardini

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Daniel Santiago
* Marcia Fortuna
* Chrystian Moté
