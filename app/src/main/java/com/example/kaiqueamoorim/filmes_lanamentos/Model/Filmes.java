package com.example.kaiqueamoorim.filmes_lanamentos.Model;



import android.graphics.Bitmap;



import java.io.Serializable;


public class Filmes implements Serializable {

//    favoritos

    private int idFAV;
    private String nameFAV;
    private String descriptionFAV;
    private double priceFAV;

//    banco


    private String email;
    private String username;
    private String password;

//    banco

//    filmes 2019


    private String definicao;


//    filmes 2019


    private int id   ;
    private String nome ;
    private String gostaram;
    private String ano;
    private String tempofilme;
    private String protagonista;
    private String pessoasFilme;
    private String descrcaoFilmes;
    private String url;
    private  transient Bitmap imagem  ;
    private float classificacao ;

    public Filmes() {
    }

////    public Filmes(int id, String nome, String gostaram, String ano, String tempofilme, String protagonista,
//                  String pessoasFilme, String descrcaoFilmes, Bitmap imagem, float classificacao) {
//
//
//
//        this.username = username;
//        this.password = password;
//
//
//
//
//        this.id = id;
//        this.nome = nome;
//        this.gostaram = gostaram;
//        this.ano = ano;
//        this.tempofilme = tempofilme;
//        this.protagonista = protagonista;
//        this.pessoasFilme = pessoasFilme;
//        this.descrcaoFilmes = descrcaoFilmes;
//        this.imagem = imagem;
//        this.classificacao = classificacao;
//    }

    public Filmes(int idFAV, String nameFAV, String descriptionFAV, double priceFAV, String email, String username, String password, String definicao, int id, String nome, String gostaram, String ano, String tempofilme, String protagonista, String pessoasFilme, String descrcaoFilmes, String url, Bitmap imagem, float classificacao) {
        this.idFAV = idFAV;
        this.nameFAV = nameFAV;
        this.descriptionFAV = descriptionFAV;
        this.priceFAV = priceFAV;
        this.email = email;
        this.username = username;
        this.password = password;
        this.definicao = definicao;
        this.id = id;
        this.nome = nome;
        this.gostaram = gostaram;
        this.ano = ano;
        this.tempofilme = tempofilme;
        this.protagonista = protagonista;
        this.pessoasFilme = pessoasFilme;
        this.descrcaoFilmes = descrcaoFilmes;
        this.url = url;
        this.imagem = imagem;
        this.classificacao = classificacao;
    }


//    banco


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    //  favoritos
    public int getIdFAV() {
        return idFAV;
    }
    public void setIdFAV(int idFAV) {
        this.idFAV = idFAV;
    }
    public String getNameFAV() {
        return nameFAV;
    }
    public void setNameFAV(String nameFAV) {
        this.nameFAV = nameFAV;
    }
    public String getDescriptionFAV() {
        return descriptionFAV;
    }
    public void setDescriptionFAV(String descriptionFAV) {
        this.descriptionFAV = descriptionFAV;
    }
    public double getPriceFAV() {
        return priceFAV;
    }
    public void setPriceFAV(double priceFAV) {
        this.priceFAV = priceFAV;
    }

//    url do yutube

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getDefinicao() {
        return definicao;
    }
    public void setDefinicao(String definicao) {
        this.definicao = definicao;
    }





    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getGostaram() {
        return gostaram;
    }
    public void setGostaram(String gostaram) {
        this.gostaram = gostaram;
    }
    public String getAno() {
        return ano;
    }
    public void setAno(String ano) {
        this.ano = ano;
    }
    public String getTempofilme() {
        return tempofilme;
    }
    public void setTempofilme(String tempofilme) {
        this.tempofilme = tempofilme;
    }
    public String getProtagonista() {
        return protagonista;
    }
    public void setProtagonista(String protagonista) {
        this.protagonista = protagonista;
    }
    public String getPessoasFilme() {
        return pessoasFilme;
    }
    public void setPessoasFilme(String pessoasFilme) {
        this.pessoasFilme = pessoasFilme;
    }
    public String getDescrcaoFilmes() {
        return descrcaoFilmes;
    }
    public void setDescrcaoFilmes(String descrcaoFilmes) {
        this.descrcaoFilmes = descrcaoFilmes;
    }
    public Bitmap getImagem() {
        return imagem;
    }
    public void setImagem(Bitmap imagem) {
        this.imagem = imagem;
    }
    public float getClassificacao() {
        return classificacao;
    }
    public void setClassificacao(float classificacao) {
        this.classificacao = classificacao;
    }

    @Override
    public String toString() {
        return this.nome;
    }



}
