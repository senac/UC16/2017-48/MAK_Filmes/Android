package com.example.kaiqueamoorim.filmes_lanamentos.Login;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.kaiqueamoorim.filmes_lanamentos.R;

public class InternetActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internet);

        WebView webView = (WebView)findViewById(R.id.internet);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);

        webView.loadUrl("www.google.com");

        webView.setWebChromeClient(new WebChromeClient(){
            public boolean naveguenoApp(WebView view, String url){
                return false;
            }
        });


    }
}
