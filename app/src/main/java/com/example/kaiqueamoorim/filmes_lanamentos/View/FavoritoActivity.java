package com.example.kaiqueamoorim.filmes_lanamentos.View;



import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.kaiqueamoorim.filmes_lanamentos.Model.Filmes;
import com.example.kaiqueamoorim.filmes_lanamentos.R;
import com.example.kaiqueamoorim.filmes_lanamentos.View.MainActivity;

import java.util.ArrayList;
import java.util.List;

public class FavoritoActivity extends AppCompatActivity {


    private ListView listView ;

    private List<String> listaFavorito ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorito);



        Intent intent = getIntent();

        listaFavorito = (List<String>) intent.getSerializableExtra(MainActivity.FAVORITO);
        if(listaFavorito != null) {

            listView = findViewById(R.id.listaFavorito);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listaFavorito);
            listView.setAdapter(adapter);

        }



    }
}
