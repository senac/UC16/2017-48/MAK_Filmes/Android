package com.example.kaiqueamoorim.filmes_lanamentos.Login;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kaiqueamoorim.filmes_lanamentos.Model.Filmes;
import com.example.kaiqueamoorim.filmes_lanamentos.DAO.BancoDeDados;
import com.example.kaiqueamoorim.filmes_lanamentos.R;
import com.example.kaiqueamoorim.filmes_lanamentos.View.MainActivity;
import com.example.kaiqueamoorim.filmes_lanamentos.View.User_Activity;


public class LoginActivity extends LoginActivity2 {
//    EditText et_username, et_password;
//    Button btn_login,btn_registrar;
//    BancoDeDados db;



    private final AppCompatActivity activity = LoginActivity.this;

    private RelativeLayout relativeLayout;

    private LinearLayout linearLayoutNome;
    private LinearLayout linearLayoutPassword;
    private LinearLayout linearLayoutConfirmPassword;

    private EditText username;
    private EditText password;
    private EditText email;

    private Button buttonLogar;
    private Button btn_registrar;
    private TextView textViewLoginLink;


    private BancoDeDados bancoDeDados = new BancoDeDados(this);
    private Filmes filmes;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();













//        db = new BancoDeDados(this);//
        username = (EditText) findViewById(R.id.Text_Nome_Login);
        password = (EditText) findViewById(R.id.Text_Senha_Login);
        this.email = (EditText) findViewById(R.id.txtEmail);
        buttonLogar = (Button)findViewById(R.id.btnLogar);
        btn_registrar = (Button)findViewById(R.id.btnRegistrar);
        btn_registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this,RegistroActivity.class);
                startActivity(intent);
            }
        });//
        buttonLogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String nome = username.getText().toString().trim() ;
                String senha = password.getText().toString().trim() ;

//
//            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//            startActivity(intent);
//

                if ( nome.equals("") || password.equals("") ) {
                    Toast.makeText(LoginActivity.this, "Favor os campos!", Toast.LENGTH_LONG).show();

                }else {
                  Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);

                }











//                    Toast.makeText(getApplicationContext(), "Bem Vindo", Toast.LENGTH_LONG).show();
//                    Intent Desligar = new Intent(this, Desligar.class);


//                Toast.makeText(LoginActivity.this, "BEM VINDO !", Toast.LENGTH_LONG).show();


//                username = (EditText) findViewById(R.id.Text_Nome_Login);
//                password = (EditText) findViewById(R.id.Text_Senha_Login);
//                email = (EditText) findViewById(R.id.txtEmail);
//
//
//                if (bancoDeDados.checkUser(email.getText().toString().trim()
//                        , password.getText().toString().trim())) {
//                    Intent accountsIntent = new Intent(activity, User_Activity.class);
//                    accountsIntent.putExtra("EMAIL", email.getText().toString().trim());
//                    emptyInputEditText();
//                    startActivity(accountsIntent);
//
////            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
////            startActivity(intent);
//                } else {
//                    Toast.makeText(LoginActivity.this, "ALGO ERRADO !", Toast.LENGTH_LONG).show();
//                }
//
//
            }
        });

    //    initViews();

      //  initObjects();
    }




    private void initViews(){
        relativeLayout = (RelativeLayout) findViewById(R.id.relativelayout);



        username = (EditText) findViewById(R.id.Text_Nome_Login);
        email = (EditText) findViewById(R.id.Text_Nome_Email);
        password = (EditText) findViewById(R.id.Text_Senha_Login);

        buttonLogar = (Button) findViewById(R.id.btnLogar);


    }



    private void initObjects(){
        bancoDeDados = new BancoDeDados(activity);

    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.btnLogar:
                verifyFromSQLite();
                break;
            case R.id.btnRegistrar:
                Intent intent = new Intent(LoginActivity.this,RegistroActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void verifyFromSQLite(){


        if (bancoDeDados.checkUser(email.getText().toString().trim()
                , password.getText().toString().trim())) {
            Intent accountsIntent = new Intent(activity, User_Activity.class);
            accountsIntent.putExtra("EMAIL", email.getText().toString().trim());
            emptyInputEditText();
            startActivity(accountsIntent);

//            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//            startActivity(intent);
        } else {
            Toast.makeText(LoginActivity.this, "ALGO ERRADO !", Toast.LENGTH_LONG).show();
        }
    }

    private void emptyInputEditText(){
        email.setText(null);
        password.setText(null);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
