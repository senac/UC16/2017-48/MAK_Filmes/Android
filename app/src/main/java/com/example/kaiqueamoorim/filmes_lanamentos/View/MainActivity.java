package com.example.kaiqueamoorim.filmes_lanamentos.View;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.kaiqueamoorim.filmes_lanamentos.Login.LoginActivity;
import com.example.kaiqueamoorim.filmes_lanamentos.Model.Filmes;
import com.example.kaiqueamoorim.filmes_lanamentos.R;
import com.example.kaiqueamoorim.filmes_lanamentos.adapter.AdapterFilme;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_NOVO = 1;


    private static Bitmap imagem;

    public static Bitmap getImagem(){
        return imagem;
    }

    public static final String FILME = "Filme";
    public static final String FAVORITO = "favorito";

    private ListView listView;
    private List<Filmes> lista = new ArrayList <>();


    private List<String> listaFavorito = new ArrayList<>();


    public void adicionarFavorito(Filmes filmes){

        if(!this.listaFavorito.contains(filmes.getNome())) {
            this.listaFavorito.add(filmes.getNome());
            Toast.makeText(this, "Adicionado aos favoritos" , Toast.LENGTH_LONG).show();

        }else{
            this.listaFavorito.remove(filmes.getNome());
            Toast.makeText(this, "Removido dos favoritos" , Toast.LENGTH_LONG).show();
        }
    }




    private AdapterFilme adapter;
    private Filmes filmesSelecionados;

//    private ListView listView2;
//    private Adapiter2019 adapiter2019;
//    private List<Filmes> lista2 = new ArrayList <>();



//    metodo de fechar a activiti direto

    @Override
    public void onBackPressed() {
        System.exit(0);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Toast.makeText(MainActivity.this, "Bem Vindo", Toast.LENGTH_LONG).show();




//      MEUS FILMES PRE DEFINIDOS



        // buscarpor


        Filmes pol = new Filmes();
        pol.getId();
        pol.setNome(getResources().getString(R.string.pol));
        pol.setGostaram(getResources().getString(R.string.pol_gostaram));
        pol.setAno(getResources().getString(R.string.pol_ano));
        pol.setTempofilme(getResources().getString(R.string.pol_tempofilme));
        pol.setProtagonista(getResources().getString(R.string.pol_protagonista));
        pol.setPessoasFilme(getResources().getString(R.string.pol_pessoasnofilme));
        pol.setDescrcaoFilmes(getResources().getString(R.string.pol_descricao));
        pol.setClassificacao(Float.parseFloat(getResources().getString(R.string.pol_classificacao)));


        pol.setImagem(BitmapFactory.decodeResource(getResources(), R.drawable.deadpool2));

        pol.setUrl(getResources().getString(R.string.urlDeadpool));


        Filmes noite = new Filmes();
        noite.getId();
        noite.setNome(getResources().getString(R.string.Noite));
        noite.setGostaram(getResources().getString(R.string.Noite_gostaram));
        noite.setAno(getResources().getString(R.string.Noite_ano));
        noite.setTempofilme(getResources().getString(R.string.Noite_tempofilme));
        noite.setProtagonista(getResources().getString(R.string.Noite_protagonista));
        noite.setPessoasFilme(getResources().getString(R.string.Noite_pessoasnofilme));
        noite.setDescrcaoFilmes(getResources().getString(R.string.Noite_descricao));
        noite.setClassificacao(Float.parseFloat(getResources().getString(R.string.Noite_classificacao)));
        noite.setImagem(BitmapFactory.decodeResource(getResources(), R.drawable.anoitedojogo));
        noite.setUrl(getResources().getString(R.string.urlANoitedoJogo));

        Filmes incriveis = new Filmes();
        incriveis.getId();
        incriveis.setNome(getResources().getString(R.string.Incríveis));
        incriveis.setGostaram(getResources().getString(R.string.Incríveis_gostaram));
        incriveis.setAno(getResources().getString(R.string.Noite_ano));
        incriveis.setTempofilme(getResources().getString(R.string.Incríveis_tempofilme));
        incriveis.setProtagonista(getResources().getString(R.string.Incríveis_protagonista_Tipo));
        incriveis.setPessoasFilme(getResources().getString(R.string.Incríveis_pessoasnofilme));
        incriveis.setDescrcaoFilmes(getResources().getString(R.string.Incríveis_descricao));
        incriveis.setClassificacao(Float.parseFloat(getResources().getString(R.string.Incríveis_classificacao)));
        incriveis.setImagem(BitmapFactory.decodeResource(getResources(), R.drawable.incrivaeis2));
        incriveis.setUrl(getResources().getString(R.string.urlIncríveis));

        Filmes venon = new Filmes();
        venon.getId();
        venon.setNome(getResources().getString(R.string.Venom));
        venon.setGostaram(getResources().getString(R.string.Venom_gostaram));
        venon.setAno(getResources().getString(R.string.Venom_ano));
        venon.setTempofilme(getResources().getString(R.string.Venom_tempofilme));
        venon.setProtagonista(getResources().getString(R.string.Venom_protagonista_Tipo));
        venon.setPessoasFilme(getResources().getString(R.string.Venom_pessoasnofilme));
        venon.setDescrcaoFilmes(getResources().getString(R.string.Venom_descricao));
        venon.setUrl(getResources().getString(R.string.urlAVenom));
        venon.setClassificacao(Float.parseFloat(getResources().getString(R.string.Venom_classificacao)));
        venon.setImagem(BitmapFactory.decodeResource(getResources(), R.drawable.venon));



        Filmes alita = new Filmes();
        alita.getId();
        alita.setNome(getResources().getString(R.string.alita));
        alita.setGostaram(getResources().getString(R.string.alita_gostaram));
        alita.setAno(getResources().getString(R.string.alita_ano));
        alita.setTempofilme(getResources().getString(R.string.alita_tempofilme));
        alita.setProtagonista(getResources().getString(R.string.alita_protagonista_Tipo));
        alita.setPessoasFilme(getResources().getString(R.string.alita_pessoasnofilme));
        alita.setDescrcaoFilmes(getResources().getString(R.string.alita_descricao));
        alita.setUrl(getResources().getString(R.string.urlalita));
        alita.setClassificacao(Float.parseFloat(getResources().getString(R.string.alita_classificacao)));
        alita.setImagem(BitmapFactory.decodeResource(getResources(), R.drawable.alita));

//        Filmes arranhaceu = new Filmes();
//        arranhaceu.getId();
//        arranhaceu.setNome(getResources().getString(R.string.Arranha_ceu));
//        arranhaceu.setGostaram(getResources().getString(R.string.Arranha_ceu_gostaram));
//        arranhaceu.setAno(getResources().getString(R.string.Arranha_ceu_ano));
//        arranhaceu.setTempofilme(getResources().getString(R.string.Arranha_ceu_tempofilme));
//        arranhaceu.setProtagonista(getResources().getString(R.string.Arranha_ceu_protagonista_Tipo));
//        arranhaceu.setPessoasFilme(getResources().getString(R.string.Arranha_ceu_pessoasnofilme));
//        arranhaceu.setDescrcaoFilmes(getResources().getString(R.string.Arranha_ceu_descricao));
//        arranhaceu.setUrl(getResources().getString(R.string.url_Arranha_ceu));
//        arranhaceu.setClassificacao(Float.parseFloat(getResources().getString(R.string.Arranha_ceu_classificacao)));
//        arranhaceu.setImagem(BitmapFactory.decodeResource(getResources(), R.drawable.arranhaceu));


        Filmes dragao = new Filmes();
        dragao.getId();
        dragao.setNome(getResources().getString(R.string.DRAGAO3));
        dragao.setGostaram(getResources().getString(R.string.DRAGAO3_gostaram));
        dragao.setAno(getResources().getString(R.string.DRAGAO3_ano));
        dragao.setTempofilme(getResources().getString(R.string.DRAGAO3_tempofilme));
        dragao.setProtagonista(getResources().getString(R.string.DRAGAO3_protagonista_Tipo));
        dragao.setPessoasFilme(getResources().getString(R.string.DRAGAO3_pessoasnofilme));
        dragao.setDescrcaoFilmes(getResources().getString(R.string.DRAGAO3_descricao));
        dragao.setUrl(getResources().getString(R.string.DRAGAO3url_));
        dragao.setClassificacao(Float.parseFloat(getResources().getString(R.string.DRAGAO3_classificacao)));
        dragao.setImagem(BitmapFactory.decodeResource(getResources(), R.drawable.comotreinaroseudragao));


        Filmes homemaranha = new Filmes();
        homemaranha.getId();
        homemaranha.setNome(getResources().getString(R.string.HOMEM_ARANHA));
        homemaranha.setGostaram(getResources().getString(R.string.HOMEM_ARANHA_gostaram));
        homemaranha.setAno(getResources().getString(R.string.HOMEM_ARANHA_ano));
        homemaranha.setTempofilme(getResources().getString(R.string.HOMEM_ARANHA_tempofilme));
        homemaranha.setProtagonista(getResources().getString(R.string.HOMEM_ARANHA_protagonista_Tipo));
        homemaranha.setPessoasFilme(getResources().getString(R.string.HOMEM_ARANHA_pessoasnofilme));
        homemaranha.setDescrcaoFilmes(getResources().getString(R.string.HOMEM_ARANHA_descricao));
        homemaranha.setUrl(getResources().getString(R.string.url_HOMEM_ARANHA));
        homemaranha.setClassificacao(Float.parseFloat(getResources().getString(R.string.HOMEM_ARANHA_classificacao)));
        homemaranha.setImagem(BitmapFactory.decodeResource(getResources(), R.drawable.homemaranhanoaranhaverso));


//        Filmes JURASSIC = new Filmes();
//        JURASSIC.getId();
//        JURASSIC.setNome(getResources().getString(R.string.JURASSIC));
//        JURASSIC.setGostaram(getResources().getString(R.string.JURASSIC_gostaram));
//        JURASSIC.setAno(getResources().getString(R.string.JURASSIC_ano));
//        JURASSIC.setTempofilme(getResources().getString(R.string.JURASSIC_tempofilme));
//        JURASSIC.setProtagonista(getResources().getString(R.string.JURASSIC_protagonista_Tipo));
//        JURASSIC.setPessoasFilme(getResources().getString(R.string.JURASSIC_pessoasnofilme));
//        JURASSIC.setDescrcaoFilmes(getResources().getString(R.string.JURASSIC_descricao));
//        JURASSIC.setUrl(getResources().getString(R.string.url_JURASSIC));
//        JURASSIC.setClassificacao(Float.parseFloat(getResources().getString(R.string.JURASSIC_classificacao)));
//        JURASSIC.setImagem(BitmapFactory.decodeResource(getResources(), R.drawable.jurassicword));

        Filmes lego = new Filmes();
        lego.getId();
        lego.setNome(getResources().getString(R.string.LEGO2));
        lego.setGostaram(getResources().getString(R.string.LEGO2_gostaram));
        lego.setAno(getResources().getString(R.string.LEGO2_ano));
        lego.setTempofilme(getResources().getString(R.string.LEGO2_tempofilme));
        lego.setProtagonista(getResources().getString(R.string.LEGO2_protagonista_Tipo));
        lego.setPessoasFilme(getResources().getString(R.string.LEGO2_pessoasnofilme));
        lego.setDescrcaoFilmes(getResources().getString(R.string.LEGO2_descricao));
        lego.setUrl(getResources().getString(R.string.url_LEGO2));
        lego.setClassificacao(Float.parseFloat(getResources().getString(R.string.LEGO2_classificacao)));
        lego.setImagem(BitmapFactory.decodeResource(getResources(), R.drawable.lego));


        Filmes maquinasmortais = new Filmes();
        maquinasmortais.getId();
        maquinasmortais.setNome(getResources().getString(R.string.MAQUINASMORTAIS));
        maquinasmortais.setGostaram(getResources().getString(R.string.MAQUINASMORTAIS_gostaram));
        maquinasmortais.setAno(getResources().getString(R.string.MAQUINASMORTAIS_ano));
        maquinasmortais.setTempofilme(getResources().getString(R.string.MAQUINASMORTAIS_tempofilme));
        maquinasmortais.setProtagonista(getResources().getString(R.string.MAQUINASMORTAIS_protagonista_Tipo));
        maquinasmortais.setPessoasFilme(getResources().getString(R.string.MAQUINASMORTAIS_pessoasnofilme));
        maquinasmortais.setDescrcaoFilmes(getResources().getString(R.string.MAQUINASMORTAIS_descricao));
        maquinasmortais.setUrl(getResources().getString(R.string.url_MAQUINASMORTAIS));
        maquinasmortais.setClassificacao(Float.parseFloat(getResources().getString(R.string.MAQUINASMORTAIS_classificacao)));
        maquinasmortais.setImagem(BitmapFactory.decodeResource(getResources(), R.drawable.maquinasmortais));


        Filmes missaoimpossivel = new Filmes();
        missaoimpossivel.getId();
        missaoimpossivel.setNome(getResources().getString(R.string.MISSÃO_IMPOSSÍVEL));
        missaoimpossivel.setGostaram(getResources().getString(R.string.MISSÃO_IMPOSSÍVEL_gostaram));
        missaoimpossivel.setAno(getResources().getString(R.string.MISSÃO_IMPOSSÍVEL_ano));
        missaoimpossivel.setTempofilme(getResources().getString(R.string.MISSÃO_IMPOSSÍVEL_tempofilme));
        missaoimpossivel.setProtagonista(getResources().getString(R.string.MISSÃO_IMPOSSÍVEL_protagonista_Tipo));
        missaoimpossivel.setPessoasFilme(getResources().getString(R.string.MISSÃO_IMPOSSÍVEL_pessoasnofilme));
        missaoimpossivel.setDescrcaoFilmes(getResources().getString(R.string.MISSÃO_IMPOSSÍVEL_descricao));
        missaoimpossivel.setUrl(getResources().getString(R.string.url_MISSÃO_IMPOSSÍVEL));
        missaoimpossivel.setClassificacao(Float.parseFloat(getResources().getString(R.string.MISSÃO_IMPOSSÍVEL_classificacao)));
        missaoimpossivel.setImagem(BitmapFactory.decodeResource(getResources(), R.drawable.missaoimpossive));


        Filmes predador = new Filmes();
        predador.getId();
        predador.setNome(getResources().getString(R.string.predador));
        predador.setGostaram(getResources().getString(R.string.predador_gostaram));
        predador.setAno(getResources().getString(R.string.predador_ano));
        predador.setTempofilme(getResources().getString(R.string.predador_tempofilme));
        predador.setProtagonista(getResources().getString(R.string.predador_protagonista_Tipo));
        predador.setPessoasFilme(getResources().getString(R.string.predador_pessoasnofilme));
        predador.setDescrcaoFilmes(getResources().getString(R.string.predador_descricao));
        predador.setUrl(getResources().getString(R.string.url_predador));
        predador.setClassificacao(Float.parseFloat(getResources().getString(R.string.predador_classificacao)));
        predador.setImagem(BitmapFactory.decodeResource(getResources(), R.drawable.predador));


        Filmes novosmutantes = new Filmes();
        novosmutantes.getId();
        novosmutantes.setNome(getResources().getString(R.string.NOVOS_MUTANTES));
        novosmutantes.setGostaram(getResources().getString(R.string.NOVOS_MUTANTES_gostaram));
        novosmutantes.setAno(getResources().getString(R.string.NOVOS_MUTANTES_ano));
        novosmutantes.setTempofilme(getResources().getString(R.string.NOVOS_MUTANTES_tempofilme));
        novosmutantes.setProtagonista(getResources().getString(R.string.NOVOS_MUTANTES_protagonista_Tipo));
        novosmutantes.setPessoasFilme(getResources().getString(R.string.NOVOS_MUTANTES_pessoasnofilme));
        novosmutantes.setDescrcaoFilmes(getResources().getString(R.string.NOVOS_MUTANTES_descricao));
        novosmutantes.setUrl(getResources().getString(R.string.url_NOVOS_MUTANTES));
        novosmutantes.setClassificacao(Float.parseFloat(getResources().getString(R.string.NOVOS_MUTANTES_classificacao)));
        novosmutantes.setImagem(BitmapFactory.decodeResource(getResources(), R.drawable.newmutants));




        lista.add(alita);
//        lista.add(JURASSIC);
        lista.add(missaoimpossivel);
        lista.add(maquinasmortais);
        lista.add(predador);
        lista.add(lego);
        lista.add(novosmutantes);
        lista.add(homemaranha);
        lista.add(dragao);
//        lista.add(arranhaceu);
        lista.add(pol);
        lista.add(noite);
        lista.add(incriveis);
        lista.add(venon);


//        Filmes 2019

//        Filmes RALPH = new Filmes();
//        RALPH.getId19();
//        RALPH.setNome19(getResources().getString(R.string.RALPH));
//        RALPH.setGostaram19(getResources().getString(R.string.RALPH_gostaram));
//        RALPH.setAno19(getResources().getString(R.string.RALPH_ano));
//        RALPH.setTempofilme19(getResources().getString(R.string.RALPH_tempofilme));
//        RALPH.setProtagonista19(getResources().getString(R.string.RALPH_protagonista_Tipo));
//        RALPH.setPessoasFilme19(getResources().getString(R.string.RALPH_pessoasnofilme));
//        RALPH.setDescrcaoFilmes19(getResources().getString(R.string.RALPH_descricao));
//        RALPH.setClassificacao19(Float.parseFloat(getResources().getString(R.string.RALPH_classificacao)));
//        RALPH.setImagem19(BitmapFactory.decodeResource(getResources(), R.drawable.ralfi));

//        add 2019
//        lista.add(RALPH);

        //        LIST DE FILMES 2019
//        listView2 = findViewById(R.id.LiataFilmes2);
//        int layout2 = android.R.layout.simple_list_item_1;
//        adapiter2019 = new Adapiter2019(this,lista2);
//        listView2.setAdapter(adapiter2019);








//      ACABAR MINHA TELA DE LIST VIEW

        listView = findViewById(R.id.LiataFilmes);
        int layout = android.R.layout.simple_list_item_1;
//        puxar o adapiter da classe adapiter
        adapter = new AdapterFilme(this,lista);
        listView.setAdapter(adapter);






        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {





            @Override
            public void onItemClick(AdapterView<?> adapter, View contexto, int posicao, long indice) {

                Filmes filmes = (Filmes) adapter.getItemAtPosition(posicao) ;
                filmesSelecionados = (Filmes) adapter.getItemAtPosition(posicao);


                Intent Urls = new Intent(Intent.ACTION_VIEW);
                Uri uri = Uri.parse(filmesSelecionados.getUrl());
                Urls.setData(uri);



                Intent intent = new Intent(MainActivity.this , DetalheActivity.class) ;
                intent.putExtra(FILME , filmes);

                imagem = filmes.getImagem() ;


                startActivity(intent);

                /*
                Intent url = new Intent(Intent.ACTION_VIEW, Uri.parse(filmesSelecionados.getUrl())) ;
                url.putExtra(FILME , filmesSelecionados);
                startActivity(url);
*/



            }




        });

//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//                Intent url = new Intent(Intent.ACTION_VIEW, Uri.parse(filmesSelecionados.getUrl())) ;
//                url.putExtra(FILME , filmesSelecionados);
//                startActivity(url);
//
//            }
//        });










    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu , menu);

        return true ;
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_NOVO){

            switch (resultCode){
                case RESULT_OK :
                    Filmes filmes = (Filmes) data.getSerializableExtra(MainActivity.FILME);
                    filmes.setImagem(NovoActivity.getImagem());
                    lista.add(filmes);
                    adapter.notifyDataSetChanged();
                    break;
                case RESULT_CANCELED :
                    Toast.makeText(this, "Cancelou" , Toast.LENGTH_LONG).show();
                    break;

            }

        }

    }


    public void u(MenuItem item) {
        Intent intent = new Intent(this , LoginActivity.class);
        startActivityForResult(intent , REQUEST_NOVO);
    }

//    public void filmes(MenuItem item) {
//        Intent intent = new Intent(this , PrincipalActivity.class);
//        startActivityForResult(intent , REQUEST_NOVO);
//    }

    public void net(MenuItem item) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://makmovies.wordpress.com/"));
        startActivity(intent);
    }



    public void favorito(MenuItem item) {


        Intent intent = new Intent(MainActivity.this , FavoritoActivity.class) ;
        intent.putExtra(FAVORITO , (Serializable) listaFavorito);


        startActivity(intent);
    }



//    public void Filmes2019(MenuItem item) {
//        Intent intent = new Intent(this , filmes_lista_2019_Activity.class);
//        startActivityForResult(intent , REQUEST_NOVO);
//    }

    public void mochila(View view) {
        Toast.makeText(MainActivity.this, "Adicionado aos Favoritos !", Toast.LENGTH_LONG).show();
    }



    public void tralerdoveno(View view) {


//        listView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent url = new Intent(Intent.ACTION_VIEW, Uri.parse(filmesSelecionados.getUrl())) ;
//                url.putExtra(FILME , filmesSelecionados);
//                startActivity(url);
//
//            }
//        });

//        Intent url = new Intent(Intent.ACTION_VIEW, Uri.parse(filmesSelecionados.getUrl())) ;
//        url.putExtra(FILME , filmesSelecionados);
//        startActivity(url);



//        listView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View posicao, int i, long l) {
////                filmesSelecionados = (Filmes) adapter.getItem(posicao);
////
////                Intent Urls = new Intent(Intent.ACTION_VIEW);
////                Uri uri = Uri.parse(filmesSelecionados.getUrl());
////                Urls.setData(uri);
//
//                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(filmesSelecionados.getUrl())) ;
//                intent.putExtra(FILME , filmesSelecionados);
//                startActivity(intent);
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
//        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapter, View contexto, int posicao, long indice) {
//
//
//                filmesSelecionados = (Filmes) adapter.getItemAtPosition(posicao);
//
//
//                Intent Urls = new Intent(Intent.ACTION_VIEW);
//                Uri uri = Uri.parse(filmesSelecionados.getUrl());
//                Urls.setData(uri);
//
//                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(filmesSelecionados.getUrl())) ;
//                intent.putExtra(FILME , filmesSelecionados);
//
//
//
//
//
//
//
//            }
//        });
//
////        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(filmesSelecionados.getUrl()));
////        startActivity(intent);
    }

}
