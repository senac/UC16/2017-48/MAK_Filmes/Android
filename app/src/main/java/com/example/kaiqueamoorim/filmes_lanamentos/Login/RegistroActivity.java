package com.example.kaiqueamoorim.filmes_lanamentos.Login;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kaiqueamoorim.filmes_lanamentos.Model.Filmes;
import com.example.kaiqueamoorim.filmes_lanamentos.DAO.BancoDeDados;
import com.example.kaiqueamoorim.filmes_lanamentos.R;

public class RegistroActivity extends RegistroActivity1 {
//    EditText username, passwrod;
//    Button btnRegistro;
//    private Filmes filmes;
//    private BancoDeDados db;



    private final AppCompatActivity activity = RegistroActivity.this;

    private RelativeLayout relativeLayout;

    private LinearLayout linearLayoutNome;
    private LinearLayout linearLayoutPassword;
    private LinearLayout linearLayoutConfirmPassword;

    private EditText textInputEditTextName;
    private EditText textInputEditTextPassword;
    private EditText textInputEditTextEmail;

    private Button buttonRegister;
    private TextView textViewLoginLink;


    private BancoDeDados bancoDeDados;
    private Filmes filmes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registroctivity);
        textInputEditTextName = findViewById(R.id.Text_Nome_Registrer);
        textInputEditTextPassword = findViewById(R.id.Text_Senha_Registre);
        textInputEditTextEmail = findViewById(R.id.Text_Nome_Email);
        buttonRegister = findViewById(R.id.btnCadastrar);
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filmes = new Filmes();
                filmes.setEmail(textInputEditTextEmail.getText().toString());
                filmes.setPassword(textInputEditTextPassword.getText().toString());
                bancoDeDados = new BancoDeDados(RegistroActivity.this);
                bancoDeDados.addUser(filmes);
                bancoDeDados.close();
                Toast.makeText(RegistroActivity.this, "Salvo com sucesso",Toast.LENGTH_LONG).show();
                finish();
            }
        });
        getSupportActionBar().hide();
        initViews();



    }

    private void initViews(){
        relativeLayout = (RelativeLayout) findViewById(R.id.relativelayout);

        textInputEditTextName = (EditText)findViewById(R.id.Text_Nome_Registrer);
        textInputEditTextEmail = (EditText) findViewById(R.id.Text_Nome_Email);
        textInputEditTextPassword = (EditText)findViewById(R.id.Text_Senha_Registre);

        buttonRegister = (Button) findViewById(R.id.btnCadastrar);
    }





    @Override
    public void onClick(View view){
        switch (view.getId()){
            case R.id.btnCadastrar:
                postDataToSQLite();
                finish();
                break;
        }
    }

    private void postDataToSQLite(){
        if (!bancoDeDados.checkUser(textInputEditTextEmail.getText().toString().trim())) {

            filmes.setEmail(textInputEditTextEmail.getText().toString().trim());
            filmes.setPassword(textInputEditTextPassword.getText().toString().trim());

            bancoDeDados.addUser(filmes);

            // Snack Bar to show success message that record saved successfully
            Toast.makeText(RegistroActivity.this, "Salvo com Sucesso !", Toast.LENGTH_LONG).show();


        } else {
            // Snack Bar to show error message that record already exists
            Toast.makeText(RegistroActivity.this, "Erro ao Salvar !", Toast.LENGTH_LONG).show();
        }
    }
}
