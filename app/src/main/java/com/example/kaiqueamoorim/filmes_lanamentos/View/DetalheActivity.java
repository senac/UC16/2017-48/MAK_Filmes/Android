package com.example.kaiqueamoorim.filmes_lanamentos.View;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.kaiqueamoorim.filmes_lanamentos.Model.Filmes;
import com.example.kaiqueamoorim.filmes_lanamentos.R;

public class DetalheActivity extends AppCompatActivity  {

    private Filmes filmes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe);


        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        Intent intent = getIntent();

        filmes = (Filmes) intent.getSerializableExtra(MainActivity.FILME);
        if(filmes != null){


            TextView textViewNome = findViewById(R.id.titulo);
            TextView textViewGostaram = findViewById(R.id.gostaram);
            TextView textViewAno = findViewById(R.id.ano);
            TextView textViewTempoFilme = findViewById(R.id.tempofilme);
            TextView textViewPotagonista = findViewById(R.id.protagonista);
            TextView textViewPessoasNoFilme = findViewById(R.id.PessoasnoFilme);
            TextView textViewDescricao = findViewById(R.id.Descricao);



            RatingBar ratingBarClassificacao = findViewById(R.id.Classificacao);
            ImageView imageView = findViewById(R.id.imageView7);


            Bitmap imagem = MainActivity.getImagem();
            filmes.setImagem(imagem);

            textViewNome.setText(filmes.getNome());
            textViewGostaram.setText(filmes.getGostaram());
            textViewAno.setText(filmes.getAno());
            textViewTempoFilme.setText(filmes.getTempofilme());
            textViewPotagonista.setText(filmes.getProtagonista());
            textViewPessoasNoFilme.setText(filmes.getPessoasFilme());
            textViewDescricao.setText(filmes.getDescrcaoFilmes());



            ratingBarClassificacao.setRating(filmes.getClassificacao());

            imageView.setImageBitmap(filmes.getImagem());


//            VideoView videoView = findViewById(R.id.VideoView);
//            String videoPath = "android.resource://" +getPackageName() + "/" + R.raw.guerrainfinta;
//            Uri uri = Uri.parse(videoPath);
//            videoView.setVideoURI(uri);
//
//
//            MediaController mediaController = new MediaController(this);
//            videoView.setMediaController(mediaController);
//            mediaController.setAnchorView(videoView);
        }




        }

}
