package com.example.kaiqueamoorim.filmes_lanamentos.Login;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.kaiqueamoorim.filmes_lanamentos.R;

public class Splash_Activity extends AppCompatActivity {


    private static int SPLASH_TIME_OUT = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_);


//        sumir a barra scrito o nome do aplicativo
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();




        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent homeintent = new Intent(Splash_Activity.this, Carregar_Activity.class);
                startActivity(homeintent);
                finish();

            }
        },SPLASH_TIME_OUT);
    }
}
