package com.example.kaiqueamoorim.filmes_lanamentos.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.kaiqueamoorim.filmes_lanamentos.Model.Filmes;


public class BancoDeDados extends SQLiteOpenHelper {

//    private static String Nome = "BancoFilmes.db";
//    private static int versao = 1;
//
//    public BancoDeDados(Context context) {
//        super(context, Nome,null,versao);
//    }
//
//
//
//    @Override
//    public void onCreate(SQLiteDatabase db) {
//        String ddl = "CREATE TABLE Filme" +
//                "(id PRIMARY KEY ," +
//                "username TEXT NOT NULL ," +
//                "password TEXT) ;";
//
//        db.execSQL(ddl);
//    }
//
//    @Override
//    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
//        String ddl = "DROP TABLE IF EXISTS Filme ;";
//        db.execSQL(ddl);
//        this.onCreate(db);
//    }
//
//    public void Salvar(Filmes filmes){
//        ContentValues values = new ContentValues();
//        values.put("username", filmes.getUsername());
//        values.put("password", filmes.getPassword());
//
//        getWritableDatabase().insert("Filme" , null , values);
//    }


    private static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "UserManager.db";

    // User table name
    private static final String TABLE_USER = "user";

    // User Table Columns names
    private static final String COLUMN_USER_ID = "user_id";
    private static final String COLUMN_USER_NAME = "user_name";
    private static final String COLUMN_USER_EMAIL = "user_email";
    private static final String COLUMN_USER_PASSWORD = "user_password";


    private String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + "(" + COLUMN_USER_ID + " INTERGER PRIMARY KEY AUTOINCREMENT," + COLUMN_USER_NAME + " TEXT," + COLUMN_USER_EMAIL + " TEXT," + COLUMN_USER_PASSWORD + " TEXT" + ")";

    private String DROP_USER_TABLE = "DROP TABLE IF EXISTS " + TABLE_USER;

    public BancoDeDados(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        Log.d("#SQL", CREATE_USER_TABLE);

        sqLiteDatabase.execSQL(CREATE_USER_TABLE);



    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(DROP_USER_TABLE);

        onCreate(sqLiteDatabase);
    }

    public void addUser(Filmes filmes) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_NAME, filmes.getUsername());
        values.put(COLUMN_USER_EMAIL, filmes.getEmail());
        values.put(COLUMN_USER_PASSWORD, filmes.getPassword());

        db.insert(TABLE_USER, null, values);
        db.close();
    }

    public void updateUser(Filmes filmes) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_NAME, filmes.getUsername());
        values.put(COLUMN_USER_EMAIL, filmes.getEmail());
        values.put(COLUMN_USER_PASSWORD, filmes.getPassword());

        db.update(TABLE_USER, values, COLUMN_USER_ID + " = ?", new String[]{String.valueOf(filmes.getId())});
        db.close();
    }

    public void deleteUser(Filmes filmes) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_USER, COLUMN_USER_ID + " = ?", new String[]{String.valueOf(filmes.getId())});
        db.close();
    }

    public boolean checkUser(String email) {
        String[] columns = {COLUMN_USER_ID};

        SQLiteDatabase db = this.getWritableDatabase();

        String selection = COLUMN_USER_EMAIL + " = ?";

        String[] selectionArgs = {email};

        Cursor cursor = db.query(TABLE_USER, columns, selection, selectionArgs, null, null, null);
        int cursorCount = cursor.getCount();
        cursor.close();

        if (cursorCount > 0) {
            return true;
        }

        return false;
    }

    public boolean checkUser(String email, String password){
        String[] columns = {
                COLUMN_USER_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();

        String selection = COLUMN_USER_EMAIL + " = ?" + " AND " + COLUMN_USER_PASSWORD + " = ?";

        String[] selectionArgs = {email, password};

        Cursor cursor = db.query(TABLE_USER, columns, selection, selectionArgs, null, null, null);
        int cursorCont = cursor.getCount();

        cursor.close();
        db.close();
        if (cursorCont > 0 ){
            return true;
        }
        return false;
    }
}
