package com.example.kaiqueamoorim.filmes_lanamentos.adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kaiqueamoorim.filmes_lanamentos.Model.Filmes;
import com.example.kaiqueamoorim.filmes_lanamentos.R;
import com.example.kaiqueamoorim.filmes_lanamentos.View.FavoritoActivity;
import com.example.kaiqueamoorim.filmes_lanamentos.View.MainActivity;

import java.util.List;


public class AdapterFilme extends BaseAdapter {

    private List<Filmes> lista;
    private Activity contexto;

    public AdapterFilme(Activity contexto , List<Filmes> lista){
        this.contexto = contexto ;
        this.lista  = lista ;
    }


    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int i) {
        return this.lista.get(i);
    }

    @Override
    public long getItemId(int id) {
        int posicao  = 0 ;

        for (int i = 0 ; i < this.lista.size() ; i++){
            if(this.lista.get(i).getId() == id){
                posicao = i ;
                break;
            }
        }

        return posicao;
    }

    @Override
    public View getView(int i, View Convertview, ViewGroup parente) {
        View view = contexto.getLayoutInflater().inflate(R.layout.activity_filmes_lista_2018, parente, false);
//      View view1 = contexto.getLayoutInflater().inflate(R.layout.activity_filmes_lista_2019_, parente, false);

        final Filmes filmes = this.lista.get(i);

        ImageView imageView = view.findViewById(R.id.imageView7);
        TextView textViewNome = view.findViewById(R.id.titulo);
        TextView textViewPessosasGostaram = view.findViewById(R.id.gostaram);
        TextView textViewAno = view.findViewById(R.id.ano);
        TextView textViewUrl = view.findViewById(R.id.Url);
        TextView textViewFavorito = view.findViewById(R.id.favoritoBtn) ;

        textViewFavorito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((MainActivity)contexto).adicionarFavorito(filmes);

            }
            });


        textViewUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent url = new Intent(Intent.ACTION_VIEW, Uri.parse(filmes.getUrl())) ;
                url.putExtra(MainActivity.FILME, filmes);

                contexto.startActivity(url);



            }
        });




        textViewNome.setText(filmes.getNome());
        textViewPessosasGostaram.setText(filmes.getGostaram());
        textViewAno.setText(filmes.getAno());
//        editTextURL.setText(filmes.getUrl());
        imageView.setImageBitmap(filmes.getImagem());






        return view;
    }


}
