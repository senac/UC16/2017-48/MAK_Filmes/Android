package com.example.kaiqueamoorim.filmes_lanamentos.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.kaiqueamoorim.filmes_lanamentos.R;

public class User_Activity extends AppCompatActivity {

    private TextView textViewName;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_);

        textViewName = (TextView) findViewById(R.id.text1);
        String nameFromIntent = getIntent().getStringExtra("EMAIL");
        textViewName.setText("Parabens, login realizado com sucesso ! " + nameFromIntent);


        button = (Button)findViewById(R.id.continueaqui);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(User_Activity.this, MainActivity.class);
                startActivity(intent);

            }
        });





    }
}
